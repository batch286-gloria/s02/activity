from django.urls import path

from . import views
urlpatterns = [
	# The path() function receive four arguments
	# We'll focus on 2 arguments that are required, 'route' and 'view'
	path('', views.index, name='index'),
	
]