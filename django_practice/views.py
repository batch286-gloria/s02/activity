from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .models import ToDoItem

def index(request):
	todoitem_list = ToDoItem.objects.all()
	context = {'todoitem_list' : todoitem_list}
	return render(request, "django_practice/index.html", context)


